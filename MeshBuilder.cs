using UnityEngine;
using System;
using System.Collections.Generic;

namespace MeshFarming
{
	[Serializable]
	public class MeshBuilder : IDisposable
	{	
		public List<MeshBuffer> buffers = new List<MeshBuffer>();
		public List<MeshBuffer> modifiedBuffers = new List<MeshBuffer>();
		public bool vertCountChanged = false;
		public bool triCountChanged = false;
		
		int vertexCount;
		int triangleCount;
		
		int[] triangles = new int[0];
		
		Vector3[] vertices = new Vector3[0];
		Vector3[] normals;
		Vector4[] tangents;
		Color[] colors;
		Vector2[] uv1;
		Vector2[] uv2;
		
		public Mesh mesh { get; private set; }
		
		public MeshBuilder ()
		{
			mesh = new Mesh();
		}
		
		public MeshBuffer GetBuffer (int verts, int tris)
		{
			var buffer = new MeshBuffer();
			buffer.index = buffers.Count;
			buffer.triId = triangleCount;
			buffer.triCount = tris;
			
			buffer.vertId = vertexCount;
			buffer.vertCount = verts;
			
			buffer.builder = this;
			
			vertexCount += verts;
			triangleCount += tris;
			
			triCountChanged = true;
			vertCountChanged = true;
			
			buffers.Add(buffer);
			
			return buffer;
		}
		
		bool trianglesDirty = false;
		bool verticesDirty = false;
		bool normalsDirty = false;
		bool tangentsDirty = false;
		bool colorsDirty = false;
		bool uv1Dirty = false;
		bool uv2Dirty = false;
		
		public void Apply ()
		{
			ApplyBufferChanges();
			ApplyMesh();
		}
		
		public void ApplyBufferChanges ()
		{
			if(vertCountChanged || triCountChanged)
			{
				if(vertCountChanged) vertices = new Vector3[vertexCount];
				if(triCountChanged) triangles = new int[triangleCount];
				foreach(var buffer in buffers)
				{
					if(vertCountChanged)
					{
						buffer.verticesDirty = true;
						buffer.normalsDirty = true;
						buffer.colorsDirty = true;
						buffer.tangentsDirty = true;
						buffer.uv1Dirty = true;
						buffer.uv2Dirty = true;
					}
					if(triCountChanged)
					{
						buffer.trianglesDirty = true;
					}
					Apply(buffer);
				} 
				modifiedBuffers.Clear();
				
				vertCountChanged = false;
				triCountChanged = false;
				
				return;
			}
			foreach(var b in modifiedBuffers)
			{
				Apply(b);
			}
			modifiedBuffers.Clear();
		}
		
		public void ApplyMesh ()
		{		
			if(verticesDirty)
			{
				mesh.vertices = vertices;
				verticesDirty = false;
			}
			if(normalsDirty)
			{
				mesh.normals = normals;
				normalsDirty = false;
			}
			if(tangentsDirty)
			{
				mesh.tangents = tangents;
				tangentsDirty = false;
			}
			if(colorsDirty)
			{
				mesh.colors = colors;
				colorsDirty = false;
			}
			if(uv1Dirty)
			{
				mesh.uv = uv1;
				uv1Dirty = false;
			}
			if(uv2Dirty)
			{
				mesh.uv2 = uv2;
				uv2Dirty = false;
			}
			if(trianglesDirty)
			{
				mesh.triangles = triangles;
				trianglesDirty = false;
			}
		}
		
		void Apply (MeshBuffer b)
		{
			if(b.trianglesDirty)
			{
				if(b.triangles == null)
				{
					triangles = null;
				}
				else if(b.triangles.Length == b.triCount)
				{
					int j = 0;
					for(int i = b.triId; i < b.triId + b.triCount; i++)
					{
						triangles[i] = b.triangles[j] + b.vertId;
						j++;
					}
					trianglesDirty = true;
				}else Debug.LogWarning("Tried to set triangle array with an invalid length on a Mesh Builder.");
				b.trianglesDirty = false;
			}
			if(b.verticesDirty)
			{
				if(b.vertices.Length == b.vertCount)
				{
					b.vertices.CopyTo (vertices, b.vertId);
					verticesDirty = true;
				}else Debug.LogWarning("Tried to set vertices array with an invalid length on a Mesh Builder.");
				b.verticesDirty = false;
			}
			if(b.normalsDirty)
			{
				if(b.normals == null)
				{
					normals = null;
				}
				else if(b.normals.Length == b.vertCount)
				{
					if(normals == null)
					{
						normals = new Vector3[vertexCount];
					}
					else if(normals.Length != vertexCount)
					{
						var newNormals = new Vector3[vertexCount];
						for(int i = 0; i < normals.Length && i < vertexCount; i++)
						{
							newNormals[i] = normals[i];
						}
						normals = newNormals;
					}
					
					b.normals.CopyTo (normals, b.vertId);
					normalsDirty = true;
				}else Debug.LogWarning("Tried to set normals array with an invalid length on a Mesh Builder.");
				b.normalsDirty = false;
			}
			if(b.tangentsDirty)
			{
				if(b.tangents == null)
				{
					tangents = null;
				}
				else if(b.tangents.Length == b.vertCount)
				{
					if(tangents == null)
					{
						tangents = new Vector4[vertexCount];
					}
					else if(tangents.Length != vertexCount)
					{
						var newTangents = new Vector4[vertexCount];
						for(int i = 0; i < tangents.Length && i < vertexCount; i++)
						{
							newTangents[i] = tangents[i];
						}			
						tangents = newTangents;
					}
					b.tangents.CopyTo (tangents, b.vertId);
					tangentsDirty = true;
				}else Debug.LogWarning("Tried to set tangents array with an invalid length on a Mesh Builder.");
				b.tangentsDirty = false;
			}
			if(b.colorsDirty)
			{
				if(b.colors == null)
				{
					colors = null;
				}
				else if(b.colors.Length == b.vertCount)
				{
					if(colors == null)
					{
						colors = new Color[vertexCount];
					}
					else if(colors.Length != vertexCount)
					{
						var newColors = new Color[vertexCount];
						for(int i = 0; i < colors.Length && i < vertexCount; i++)
						{
							newColors[i] = colors[i];
						}					
						colors = newColors;
					}
					b.colors.CopyTo (colors, b.vertId);
					colorsDirty = true;
				}else Debug.LogWarning("Tried to set colors array with an invalid length on a Mesh Builder.");
				b.colorsDirty = false;
			}
			if(b.uv1Dirty)
			{
				if(b.uv1 == null)
				{
					uv1 = null;
				}
				else if(b.uv1.Length == b.vertCount)
				{
					if(uv1 == null)
					{
						uv1 = new Vector2[vertexCount];
					}
					else if(uv1.Length != vertexCount)
					{
						var newUv1 = new Vector2[vertexCount];
						for(int i = 0; i < uv1.Length && i < vertexCount; i++)
						{
							newUv1[i] = uv1[i];
						}					
						uv1 = newUv1;
					}
					b.uv1.CopyTo (uv1, b.vertId);
					uv1Dirty = true;
				}else Debug.LogWarning("Tried to set uv1 array with an invalid length on a Mesh Builder.");
				b.uv1Dirty = false;
			}
			if(b.uv2Dirty)
			{
				if(b.uv2 == null)
				{
					uv2 = null;
				}
				else if(b.uv2.Length == b.vertCount)
				{
					if(uv2 == null)
					{
						uv2 = new Vector2[vertexCount];
					}
					else if(uv2.Length != vertexCount)
					{
						var newUv2 = new Vector2[vertexCount];
						for(int i = 0; i < uv2.Length && i < vertexCount; i++)
						{
							newUv2[i] = uv2[i];
						}					
						uv2 = newUv2;
					}
					b.uv2.CopyTo (uv2, b.vertId);
					uv2Dirty = true;
				}else Debug.LogWarning("Tried to set uv2 array with an invalid length on a Mesh Builder.");
				b.uv2Dirty = false;
			}
		}
		
		public void MarkDirty (MeshBuffer buffer)
		{
			modifiedBuffers.Add(buffer);
		}
		
		public void Dispose ()
		{
			triangles = null;
			vertices = null;
			normals = null;
			tangents = null;
			colors = null;
			uv1 = null;
			uv2 = null;
			GameObject.DestroyImmediate(mesh);
			mesh = null;
		}
	}
}