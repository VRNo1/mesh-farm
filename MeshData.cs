using UnityEngine;
using System;

namespace MeshFarming
{
	public struct MeshData
	{
		public int[] triangles;

		Vector3[] _vertices;
		public Vector3[] vertices
		{
			get{ return _vertices; }
			set{
				_vertices = value;
				RecalculateBounds();
			}
		}

		public Vector3[] normals;
		public Vector4[] tangents;
		public Vector2[] uv1;
		public Vector2[] uv2;
		public Color[] colors;
		
		public Bounds bounds;
		
		public MeshData (int[] triangles, Vector3[] vertices)
		{
			this.vertices = vertices;
			this.triangles = triangles;
			normals = null;
			tangents = null;
			uv1 = null;
			uv2 = null;
			colors = null;
		}
		
		public MeshData (Mesh mesh)
		{
			this.vertices = mesh.vertices;
			this.triangles = mesh.triangles;
			
			this.normals = mesh.normals;
			if(this.normals.Length != this.vertices.Length) this.normals = null;
			
			this.tangents = mesh.tangents;
			if(this.tangents.Length != this.vertices.Length) this.tangents = null;
			
			this.uv1 = mesh.uv1;
			if(this.uv1.Length != this.vertices.Length) this.uv1 = null;
			
			this.uv2 = mesh.uv2;
			if(this.uv2.Length != this.vertices.Length) this.uv2 = null;
			
			this.colors = mesh.colors;
			if(this.colors.Length != this.vertices.Length) this.colors = null;
		}
		
		public void Multiply (Matrix4x4 matrix)
		{
			bool doNormals = normals != null && normals.Length != 0;
			bool doTangents = tangents != null && tangents.Length != 0;
			int vertLength = vertices.Length;
			for(int i = 0; i < vertLength; i++)
			{
				vertices[i] = matrix.MultiplyPoint(vertices[i]);
				if(doNormals) normals[i] = matrix.MultiplyVector(normals[i]);
				if(doTangents) tangents[i] = matrix.MultiplyVector(tangents[i]);
			}
		}
		
		public void RecalculateBounds ()
		{
			if(vertices == null || vertices.Length == 0)
			{
				bounds = new Bounds();
				return;
			}
			bounds = new Bounds(vertices[0],Vector3.zero);
			for(int i = 1; i < vertices.Length; i++)
			{
				bounds.Encapsulate(vertices[i]);
			}
		}

		public void SetMesh (Mesh mesh)
		{
			mesh.vertices = vertices;
			mesh.triangles = triangles;
			mesh.normals = normals;
			mesh.tangents = tangents;
			mesh.colors = colors;
			mesh.uv = uv1;
			mesh.uv2 = uv2;
		}
	}
}