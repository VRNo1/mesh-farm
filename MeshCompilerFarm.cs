using UnityEngine;
using System.Collections.Generic;

namespace MeshFarming
{
	public class MeshCompilerFarm : Dictionary<MeshAttributes, MeshCompiler>
	{
		public MeshCompiler GetOrCreate (MeshAttributes attributes)
		{
			if(ContainsKey(attributes)) return this[attributes];
			var c = new MeshCompiler();
			Add(attributes, c);
			return c;
		}
	}
}