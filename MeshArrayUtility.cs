using UnityEngine;

namespace MeshFarming
{
	public class MeshArrayUtility 
	{
		public static T[] GetArray<T> (T value, int length) where T : struct
		{
			var arr = new T[length];
			for(int i = 0; i < length; i++) arr[i] = value;
			return arr;
		}
		
		public static void SetArray <T> (T[] arr, T value)
		{
			for(int i = 0; i < arr.Length; i++) arr[i] = value;
		}
	}
}